//
//  SecondVC.swift
//  Project1
//
//  Created by Mukhammadyunus on 8/27/20.
//  Copyright © 2020 Rakhmatillo Topiboldiev. All rights reserved.
//

import UIKit

class SecondVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        makeScreen(view: self.view, shouldMakeScreen: true)
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        makeScreen(view: self.view, shouldMakeScreen: false)
    }
    

}
